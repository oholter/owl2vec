package io;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.google.common.cache.Cache;

public class ParameterReader {
	
			// general parameters
			public String walks_system = "owl2vec+"; 
			public String temp_ontology_file = "/home/ole/master/test_onto/tmp_onto_file.txt";
			public String temp_graph_file = "/home/ole/master/test_onto/tmp_graph_file.txt";
			public int number_of_threads = 12;

			// walk parameters
			public int number_of_walks = 100;
			public String embeddings_representation = "full_uri"; 
			public boolean include_edges = false;
			public int walk_depth = 40;
			public int limit = 100000;
			public int offset = 0;
			public boolean cache_edge_weights = true;
			public int child_limit = 100;
			public boolean include_individuals = false;


			public double p = 1.2;
			public double q = 0.5;

			// embeddings parameters
			public int window_size = 10;
			public int dimensions = 100;
			public int negative_samples = 25;
			public String embeddings_system = "word2vec";
			public int epochs = 5;
			public String python3_path = "/home/ole/anaconda/bin/python";
			
			public String anchors_source;
			public String logmap_output_path;
	
	private static ParameterReader instance = null;
	
	private ParameterReader () {
		readParameters();
	}
	
	public static ParameterReader getInstance() {
		if (instance == null) {
			instance = new ParameterReader();
		}
		return instance;
	}
	
	public void readParameters() {
		
		try {
			String fileName = "config.properties";
			InputStream parameterStream = getClass().getClassLoader().getResourceAsStream(fileName);
			Properties properties = new Properties();
			properties.load(parameterStream);
			
			
			temp_ontology_file = properties.getProperty("temp_ontology_file").trim();
			temp_graph_file = properties.getProperty("temp_graph_file").trim();
			
			
			walks_system = properties.getProperty("walks_system").trim();
			number_of_threads = Integer.parseInt(properties.getProperty("number_of_threads"));
			number_of_walks = Integer.parseInt(properties.getProperty("number_of_walks"));
			embeddings_representation = properties.getProperty("embeddings_representation").trim();
			include_edges = Boolean.parseBoolean(properties.getProperty("include_edges"));
			walk_depth = Integer.parseInt(properties.getProperty("walk_depth"));
			limit = Integer.parseInt(properties.getProperty("limit"));
			offset = Integer.parseInt(properties.getProperty("offset"));
			cache_edge_weights = Boolean.parseBoolean(properties.getProperty("cache_edge_weights"));
			child_limit = Integer.parseInt(properties.getProperty("child_limit"));
			include_individuals = Boolean.parseBoolean(properties.getProperty("include_individuals"));

			p = Double.parseDouble(properties.getProperty("p"));
			q = Double.parseDouble(properties.getProperty("q"));


			window_size = Integer.parseInt(properties.getProperty("window_size"));
			dimensions = Integer.parseInt(properties.getProperty("dimensions"));
			negative_samples = Integer.parseInt(properties.getProperty("negative_samples"));
			embeddings_system = properties.getProperty("embeddings_system").trim();
			epochs = Integer.parseInt(properties.getProperty("epochs"));
			python3_path = properties.getProperty("python3_path").trim();
			
			anchors_source = properties.getProperty("anchors_source").trim();
			logmap_output_path = properties.getProperty("logmap_output_path").trim();

			System.out.println("Parameters read");
		} catch (NumberFormatException ne) {
			System.out.println("Malformed parameter!");
			ne.printStackTrace();
			System.exit(1);
		}
		catch (Exception e) {
			System.out.println("parameter.txt not found, using standard parameters");
		}
	}
	
	public static void main(String[] args) {
		ParameterReader params = ParameterReader.getInstance();
		System.out.println("temp_ontology_file: " + params.temp_ontology_file);
		System.out.println("temp_graph_file: " + params.temp_graph_file);
		System.out.println("walks_system: " + params.walks_system);
		System.out.println("number_of_threads: " + params.number_of_threads);
		
		System.out.println("number_of_walks: " + params.number_of_walks);
		System.out.println("embeddings representation: " + params.embeddings_representation);
		System.out.println("include_edges: " + params.include_edges);
		System.out.println("Walks depth: " + params.walk_depth);
		System.out.println("limit: " + params.limit);
		System.out.println("offset: " + params.offset);
		System.out.println("cache_edge_weights " + params.cache_edge_weights);
		System.out.println("child_limit: " + params.child_limit);
		System.out.println("include_individuals: " + params.include_individuals);
		
		System.out.println("p: " + params.p);
		System.out.println("q: " + params.q);
		
		System.out.println("window_size: " + params.window_size);
		System.out.println("dimensions: " + params.dimensions);
		System.out.println("negative_samples: " + params.negative_samples);
		System.out.println("embeddings_system: " + params.embeddings_system);
		System.out.println("epochs: " + params.epochs);
		System.out.println("python3_path: " + params.python3_path);
		System.out.println("anchors_source: " + params.anchors_source);
		System.out.println("logmap_output_path: " + params.logmap_output_path);

	}
}
