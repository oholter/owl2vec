package utils;

import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import uk.ac.ox.krr.logmap2.mappings.objects.MappingObjectStr;

public class OntologyUtils {
	public static void addAnchor(OWLOntology onto, MappingObjectStr mapping) {
		try {
			addAnchor(onto, mapping.getIRIStrEnt1(), mapping.getIRIStrEnt2());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addAllAnchors(OWLOntology onto, Set<MappingObjectStr> mappings) {
		int numMappings = 0;
		for (MappingObjectStr mapping : mappings) {
			addAnchor(onto, mapping);
			numMappings++;
		}
		System.out.println("Total: " + numMappings + " anchors");
	}

	public static void addAnchor(OWLOntology mergedOnto, String firstIRI, String secondIRI) throws Exception {
		OWLOntologyManager man = OWLManager.createOWLOntologyManager();
		OWLOntology anchorOntology = man.createOntology();
		OWLDataFactory factory = man.getOWLDataFactory();

		IRI iri = IRI.create(firstIRI);
		boolean isClass = mergedOnto.containsClassInSignature(iri);
		boolean isObjectProperty = mergedOnto.containsObjectPropertyInSignature(iri);
		boolean isDataProperty = mergedOnto.containsDataPropertyInSignature(iri);
//		System.out.println("is class: " + isClass);
//		System.out.println("is object property: " + isObjectProperty);
//		System.out.println("is data property: " + isDataProperty);

		if (isClass) {
			OWLClass cl1 = factory.getOWLClass(IRI.create(firstIRI));
			OWLClass cl2 = factory.getOWLClass(IRI.create(secondIRI));
			OWLAxiom ax = factory.getOWLEquivalentClassesAxiom(cl1, cl2);
			man.addAxiom(anchorOntology, ax);
		} else if (isObjectProperty) {
			OWLObjectProperty p1 = factory.getOWLObjectProperty(IRI.create(firstIRI));
			OWLObjectProperty p2 = factory.getOWLObjectProperty(IRI.create(secondIRI));
			OWLAxiom ax = factory.getOWLEquivalentObjectPropertiesAxiom(p1, p2);
			man.addAxiom(anchorOntology, ax);
		} else if (isDataProperty) {
			OWLDataProperty p1 = factory.getOWLDataProperty(IRI.create(firstIRI));
			OWLDataProperty p2 = factory.getOWLDataProperty(IRI.create(secondIRI));
			OWLAxiom ax = factory.getOWLEquivalentDataPropertiesAxiom(p1, p2);
			man.addAxiom(anchorOntology, ax);
		} else {
//			log.warn("Not able to find type of axiom");
		}
		addAnchorsToOntology(mergedOnto, anchorOntology);
	}
	
	public static void addAnchorsToOntology(OWLOntology onto, OWLOntology anchorOntology) throws Exception {
		if (anchorOntology == null) {
			OWLOntologyManager man = OWLManager.createOWLOntologyManager();
			anchorOntology = man.createOntology();
		}
		OWLOntologyManager mergedManager = OWLManager.createOWLOntologyManager();
		Set<OWLEquivalentClassesAxiom> equivalentClasses = anchorOntology.getAxioms(AxiomType.EQUIVALENT_CLASSES);
		Set<OWLEquivalentObjectPropertiesAxiom> equivalentObjectProperties = anchorOntology
				.getAxioms(AxiomType.EQUIVALENT_OBJECT_PROPERTIES);
		Set<OWLEquivalentDataPropertiesAxiom> equivalentDataProperties = anchorOntology
				.getAxioms(AxiomType.EQUIVALENT_DATA_PROPERTIES);
		Set<OWLSubClassOfAxiom> subClassOf = anchorOntology.getAxioms(AxiomType.SUBCLASS_OF);

		int numAnchors = 0;

		for (OWLEquivalentClassesAxiom eqClassAxiom : equivalentClasses) {
			mergedManager.addAxiom(onto, eqClassAxiom);
//			System.out.println(eqClassAxiom);
			numAnchors++;
		}
		for (OWLEquivalentObjectPropertiesAxiom eqObjectPropertyAxiom : equivalentObjectProperties) {
			mergedManager.addAxiom(onto, eqObjectPropertyAxiom);
//			System.out.println(eqObjectPropertyAxiom);
			numAnchors++;
		}
		for (OWLEquivalentDataPropertiesAxiom eqDataPropertyAxiom : equivalentDataProperties) {
			mergedManager.addAxiom(onto, eqDataPropertyAxiom);
//			System.out.println(eqDataPropertyAxiom);
			numAnchors++;
		}
		for (OWLSubClassOfAxiom subClAxiom : subClassOf) {
			mergedManager.addAxiom(onto, subClAxiom);
			System.out.println(subClAxiom);
			numAnchors++;
		}
		System.out.println("Added " + numAnchors + " anchors");
	}
}
