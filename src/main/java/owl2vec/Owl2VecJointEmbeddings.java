/**
 * This file is part of OWL2Vec
 * 
 * @author Ole Magnus Holter
 */
package owl2vec;

import java.io.File;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.semanticweb.owlapi.model.OWLOntology;

import embeddings_trainer.EmbeddingsTrainer;
import io.AlignmentsReader;
import io.OAEIAlignmentsReader;
import io.OntologyReader;
import io.ParameterReader;
import ontology_projector.OntologyProjector;
import uk.ac.ox.krr.logmap2.LogMap2_Matcher;
import uk.ac.ox.krr.logmap2.mappings.objects.MappingObjectStr;
import utils.OntologyUtils;
import walks_generator.Owl2VecPlusWalksGenerator;
import walks_generator.WalksGenerator;

public class Owl2VecJointEmbeddings extends Owl2Vec {
	private String secondOntology;
	private Set<MappingObjectStr> anchorSet;
	private ParameterReader parameters;

	public Owl2VecJointEmbeddings(String firstOntology, String secondOntology, String outputFile, String tmpOntoFile,
			String tmpGraphFile, Set<MappingObjectStr> anchorSet) {
		super(firstOntology, outputFile);
		this.secondOntology = secondOntology;
		System.out.println("first: " + firstOntology);
		System.out.println("second: " + secondOntology);
		parameters = ParameterReader.getInstance();
		this.anchorSet = anchorSet;
	}

	public void createEmbeddings() {
		String firstOntologyFile = inputFile;
		String secondOntologyFile = secondOntology;

		String currentDir = new File(ClassLoader.getSystemClassLoader().getResource("").getPath()).toString();

		OntologyReader reader = new OntologyReader();
		reader.setFname(firstOntologyFile);
		try {
			log.info("Reading onto in file: " + firstOntologyFile);
			reader.readOntology();
		} catch (Exception e) {
			e.printStackTrace();
		}
		OWLOntology onto1 = reader.getOntology();
		log.info("Read onto: " + firstOntologyFile);

		reader.setFname(secondOntologyFile);
		try {
			log.info("Reading onto in file: " + secondOntologyFile);
			reader.readOntology();
		} catch (Exception e) {
			e.printStackTrace();
		}
		OWLOntology onto2 = reader.getOntology();
		log.info("Read onto: " + secondOntologyFile);

		OWLOntology mergedOnto;
		try {
			mergedOnto = OntologyReader.mergeOntologies("merged", new OWLOntology[] { onto1, onto2 });
			OntologyUtils.addAllAnchors(mergedOnto, anchorSet);
			OntologyReader.writeOntology(mergedOnto, "file:" + parameters.temp_ontology_file, "owl");
		} catch (Exception e) {
			e.printStackTrace();
		}

		OntologyProjector projector = new OntologyProjector("file:" + parameters.temp_ontology_file);
		try {
			projector.projectOntology();
			projector.saveModel(parameters.temp_graph_file);
		} catch (Exception e) {
			e.printStackTrace();
		}

		WalksGenerator walksGenerator = new Owl2VecPlusWalksGenerator(parameters.temp_graph_file, outputFile,
				parameters.number_of_threads, parameters.walk_depth, parameters.limit, parameters.number_of_walks,
				parameters.offset, parameters.p, parameters.q, parameters.embeddings_representation,
				parameters.include_individuals, parameters.include_edges, parameters.cache_edge_weights);

		walksGenerator.generateWalks();
		log.info("Finished generating walks");
		String walksFile = outputFile;

		EmbeddingsTrainer trainer = new EmbeddingsTrainer(walksFile, outputFile);

		try {
			trainer.trainEmbeddings();
			log.info("Finished training\nWritten to file: " + outputFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * arguments:
	 * 
	 * <first_ontology> <second_ontology> <anchor source>
	 * 
	 * where <anchor source> is either logmap_full, logmap_anchors or a file path
	 * 
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();
		ParameterReader parameters = ParameterReader.getInstance();

		/** defining input/output and temporary working files */
		String tmpOntoFile = parameters.temp_ontology_file;
		String tmpGraphFile = parameters.temp_graph_file;
		String logMapOutputPath = "/home/ole/master/test_onto/logmap_out/";

		if (args.length < 3) {
			System.out.println(
					"OWL2VecJointEmbeddings missing arguments:\nUsage: <first ontology file> <second ontology file> <output file>");
			System.out.println("example: /home/person/ontologyA.owl /home/person/ontologB.owl /home/person/output.txt");
		}

		else {
			String firstOntologyFile = args[0];
			String secondOntologyFile = args[1];
			String outputFile = args[2];

			

			Set<MappingObjectStr> anchors = null;

			if (parameters.anchors_source.equals("logmap_anchors")) {
				LogMap2_Matcher logMapMatcher = new LogMap2_Matcher("file:" + firstOntologyFile,
						"file:" + secondOntologyFile, logMapOutputPath, true);
				anchors = logMapMatcher.getLogmap2_anchors();
			} else if (parameters.anchors_source.equals("logmap_full")) {
				LogMap2_Matcher logMapMatcher = new LogMap2_Matcher("file:" + firstOntologyFile,
						"file:" + secondOntologyFile, logMapOutputPath, true);
				anchors = logMapMatcher.getLogmap2_Mappings();
			} else {
				AlignmentsReader reader = new OAEIAlignmentsReader(parameters.anchors_source);
				anchors = reader.getMappingsAsSet();
			} 

			Owl2VecJointEmbeddings owl2vec = new Owl2VecJointEmbeddings(firstOntologyFile, secondOntologyFile,
					outputFile, tmpOntoFile, tmpGraphFile, anchors);
			owl2vec.createEmbeddings();
		}
	}
}
