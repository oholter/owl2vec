/**
 * This file is part of OWL2Vec
 * @author Ole Magnus Holter
 */
package owl2vec;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import embeddings_trainer.EmbeddingsTrainer;
import io.ParameterReader;
import ontology_projector.OntologyProjector;
import utils.TestRunUtils;
import walks_generator.Owl2VecPlusWalksGenerator;
import walks_generator.WalksGenerator;

public class Owl2Vec {

	protected static final Logger log = LoggerFactory.getLogger(Owl2VecPlusWalksGenerator.class);

	protected String inputFile;
	protected String outputFile;

	ParameterReader parameters;

	public Owl2Vec(String inputFile, String outputFile) {
		this.inputFile = inputFile;
		this.outputFile = outputFile;
		parameters = ParameterReader.getInstance();
	}

	public void createEmbeddings() {
		OntologyProjector projector = new OntologyProjector(inputFile);

		try {
			projector.projectOntology();
			projector.saveModel(TestRunUtils.modelPath);
			log.info("projection saved");
		} catch (Exception e) {
			e.printStackTrace();
		}

		WalksGenerator walksGenerator = new Owl2VecPlusWalksGenerator(TestRunUtils.modelPath, outputFile,
				parameters.number_of_threads, parameters.walk_depth, parameters.limit, parameters.number_of_walks,
				parameters.offset, parameters.p, parameters.q, parameters.embeddings_representation,
				parameters.include_individuals, parameters.include_edges, parameters.cache_edge_weights);

		walksGenerator.generateWalks();
		log.info("Finished generating walks");
		String walksFile = outputFile;

		EmbeddingsTrainer trainer = new EmbeddingsTrainer(walksFile, outputFile);

		try {
			trainer.trainEmbeddings();
			log.info("Finished training\nWritten to file: " + outputFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		if (args.length < 2) {
			System.out.println("Owl2Vec:\nUsage: <input> <output> ");
			System.out.println("example: /home/person/ontology.owl /home/person/output.txt");
		} else {
			Owl2Vec owl2vec = new Owl2Vec("file:" + args[0], args[1]);
			owl2vec.createEmbeddings();
		}
	}
}
