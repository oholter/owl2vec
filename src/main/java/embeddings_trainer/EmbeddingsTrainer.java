package embeddings_trainer;

import java.nio.file.Path;
import java.nio.file.Paths;

import io.ParameterReader;

/**
 * This class calls python script to train the embeddings from the file. The
 * python3 path with gensim must be provided
 * 
 * @author ole
 *
 */
public class EmbeddingsTrainer {

	private ParameterReader parameters;

	private String walksFile;
	private String embeddingsFile;

	public EmbeddingsTrainer(String walksFile, String embeddingsFile) {
		parameters = ParameterReader.getInstance();
		this.walksFile = walksFile;
		this.embeddingsFile = embeddingsFile;
	}

	/**
	 * 
	 * @param model: the model to use for training
	 */
	public void trainEmbeddings() throws Exception {

		String[] command = null;
		String model = parameters.embeddings_system;
		
		if (model.equals("word2vec")) {
			Path word2vecScriptPath = Paths.get(getClass().getClassLoader().getResource("learn_document.py").toURI());
			System.out.println("Running Word2Vec");
			command = new String[8];
			command[0] = parameters.python3_path;
			command[1] = word2vecScriptPath.toString();
			command[2] = walksFile;
			command[3] = embeddingsFile;
			command[4] = Integer.toString(parameters.dimensions);
			command[5] = Integer.toString(parameters.window_size);
			command[6] = Integer.toString(parameters.epochs);
			command[7] = Integer.toString(parameters.negative_samples);
		} else if (model.equals("twodocumentlabels")) {
			Path labelWordsScriptPath = Paths
					.get(getClass().getClassLoader().getResource("learn_label_words.py").toURI());
			command = new String[8];
			command[0] = parameters.python3_path;
			command[1] = labelWordsScriptPath.toString();
			command[2] = walksFile;
			command[3] = embeddingsFile;
			command[4] = Integer.toString(parameters.dimensions);
			command[5] = Integer.toString(parameters.window_size);
			command[6] = Integer.toString(parameters.epochs);
			command[7] = Integer.toString(parameters.negative_samples);
		} else if (model.toLowerCase().equals("fasttext")) {
			System.out.println("Running fasttext");
			Path fasttextScriptPath = Paths
					.get(getClass().getClassLoader().getResource("fasttext_learn_document.py").toURI());
			command = new String[8];
			command[0] = parameters.python3_path;
			command[1] = fasttextScriptPath.toString();
			command[2] = walksFile;
			command[3] = embeddingsFile;
			command[4] = Integer.toString(parameters.dimensions);
			command[5] = Integer.toString(parameters.window_size);
			command[6] = Integer.toString(parameters.epochs);
			command[7] = Integer.toString(parameters.negative_samples);
		}

		/*
		 * else if (model.toLowerCase().equals("starspace")) {
		 * System.out.println("Running starspace"); String cmdString =
		 * "/home/ole/master/StarSpace/starspace " + "train " +
		 * "-trainFile /home/ole/master/test_onto/walks_out.txt " +
		 * "-model /home/ole/master/test_onto/cache/starspace.model " + "-dim 50 " +
		 * "-loss hinge " + "-thread 10 " + "-similarity cosine " // only used for hinge
		 * + "-minCount 1 " + "-ngrams 2 " + "-trainMode 5 " + "-epoch 1 " +
		 * "-maxNegSamples 25 " + "-lr 0.01" // learning // rate + "-ws 5"; // windows
		 * 
		 * command = cmdString.split(" "); }
		 */

		try {
			Process pr = new ProcessBuilder().command(command).inheritIO().start();
			pr.waitFor();
//		    System.out.println(pr.exitValue());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void main(String[] args) throws Exception {
		EmbeddingsTrainer trainer = new EmbeddingsTrainer("/home/ole/master/test_onto/walks_out.txt",
				"/home/ole/master/test_onto/output.bin");
		trainer.trainEmbeddings();
	}
}
